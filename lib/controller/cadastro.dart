import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ControllerCadastro{

  tentarCadastro(GlobalKey<FormState> _form, context){
    final form = _form.currentState;
    form.save();
    if(form.validate()){
      Navigator.pop(context);
    }
  }

}