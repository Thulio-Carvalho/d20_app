import 'dart:async';

import 'package:d20_app/utils/network_util.dart';
import 'package:d20_app/model/user.dart';

class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL = "https://d20-app.herokuapp.com/";
  static final LOGIN_URL = BASE_URL + "/user";


  // TODO check POST parameters
  Future<User> login(String username, String password) {
    return _netUtil.post(LOGIN_URL, body: {
      "username": username,
      "password": password
    }).then((dynamic res) {
      print(res.toString());
      if(res["error"]) throw new Exception(res["error_msg"]);
      return new User.fromJSON(res["user"]);
    });
  }
}