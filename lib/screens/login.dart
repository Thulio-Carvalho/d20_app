import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../repository/user_repository.dart';
import '../model/user.dart';
import '../utils/validators.dart';
import '../controller/login.dart';
import 'package:d20_app/screens/home.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  List<User> _users = <User>[];
  String _password;
  String _email;
  final _formKey = GlobalKey<FormState>();
  Validator _validator;
  ControllerLogin _controllerLogin;

  @override
  void initState() {
    super.initState();
    listenForUsers();
  }

  void listenForUsers() async {
    final Stream<User> stream = await getUsers();
    stream.listen((User user) => setState(() => _users.add(user)));
  }

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  Widget build(BuildContext context) {
    _validator = new Validator();
    _controllerLogin = new ControllerLogin();

    final emailField = TextFormField(
      obscureText: false,
      onSaved: (value) => _email = value,
      style: TextStyle(fontSize: 15),
      validator: _validator.emailValidator,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "E-mail",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(16.0))),
    );

    final passwordField = TextFormField(
      obscureText: true,
      onSaved: (value) => _password = value,
      style: TextStyle(fontSize: 15),
      validator: _validator.senhaValidator,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Senha",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(16.0))),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(5.0),
      color: Color(0xffffffff),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () => {
          _controllerLogin.tentarLogin(_formKey),
          Navigator.pushNamed(context, '/home')
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.black54, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Center(
          child: Center(
            child: Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(60.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 155.0,
                          child: Image.asset(
                            "assets/d20-logo.png",
                            fit: BoxFit.contain,
                          ),
                        ),
                        Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 45.0),
                              emailField,
                              SizedBox(height: 25.0),
                              passwordField,
                              SizedBox(
                                height: 35.0,
                              ),
                              Padding(
                                  child: loginButon,
                                  padding: const EdgeInsets.fromLTRB(
                                      40.0, 0.0, 40.0, 0.0)),
                              SizedBox(
                                height: 15.0,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Ainda não é cadastrado? ",
                          style: TextStyle(fontSize: 18)),
                      InkWell(
                        onTap: () => Navigator.pushNamed(context, '/cadastro'),
                        child: Text(
                          "Junte-se a nós!",
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.lightBlue,
                              decoration: TextDecoration.underline),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
