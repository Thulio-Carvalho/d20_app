import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../repository/user_repository.dart';
import '../model/user.dart';
import '../utils/validators.dart';
import '../controller/cadastro.dart';

class CadastroPage extends StatefulWidget {
  CadastroPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CadastroPageState createState() => _CadastroPageState();
}

class _CadastroPageState extends State<CadastroPage> {

  List<User> _users = <User>[];
  String _nome;
  String _sobrenome;
  String _telefone;
  String _email;
  final _formKey = GlobalKey<FormState>();
  Validator _validator;
  ControllerCadastro _controllerCadastro;

  @override
  void initState() {
    super.initState();
    listenForUsers();
  }

  void listenForUsers() async {
    final Stream<User> stream = await getUsers();
    stream.listen((User user) =>
        setState(() =>  _users.add(user))
    );
  }

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  Widget build(BuildContext context) {

    _validator = new Validator();
    _controllerCadastro = new ControllerCadastro();

    final emailField = TextFormField(
      obscureText: false,
      onSaved: (value) => _email = value,
      style: TextStyle(fontSize: 15),
      validator: _validator.emailValidator,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "E-mail",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(16.0))),
    );

    final nomeField = TextFormField(
      onSaved: (value) => _nome = value,
      style: TextStyle(fontSize: 15),
      validator: _validator.nomeValidator,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Nome",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(16.0))),
    );

    final sobreNomeField = TextFormField(
      onSaved: (value) => _sobrenome = value,
      style: TextStyle(fontSize: 15),
      validator: _validator.sobrenomeValidator,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Sobrenome",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(16.0))),
    );

    final telefoneField = TextFormField(
      onSaved: (value) => _telefone = value,
      style: TextStyle(fontSize: 15),
      validator: _validator.telefoneValidator,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Telefone",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(16.0))),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(5.0),
      color: Color(0xffffffff),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () => _controllerCadastro.tentarCadastro(_formKey, context),
        child: Text("Cadastrar",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.black54, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Center(
          child: Center(
            child: Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 155.0,
                          child: Image.asset(
                            "assets/d20-logo-simple.png",
                            fit: BoxFit.contain,
                          ),
                        ),
                        SizedBox(height: 45.0),
                        Text("Cadastre-se", style: TextStyle(fontSize: 40, color: Colors.black45),),
                        SizedBox(height: 20.0),
                        Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget> [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                    child: nomeField,
                                  ),
                                  SizedBox(width: 20.0,),
                                  Expanded(
                                    child: sobreNomeField,
                                  ),
                                ],
                              ),
                              SizedBox(height: 25.0),
                              emailField,
                              SizedBox(height: 25.0),
                              telefoneField,
                              SizedBox(
                                height: 20.0,
                              ),
                              Padding(
                                  child: loginButon,
                                  padding: const EdgeInsets.fromLTRB(70.0, 0.0, 70.0, 0.0)
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
    );
  }
}
