import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../repository/user_repository.dart';
import '../model/user.dart';
import '../utils/validators.dart';
import '../controller/login.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void initState() {
    super.initState();
  }

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  Widget build(BuildContext context) {

    final logoutButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(5.0),
      color: Color(0xffffffff),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () => Navigator.pushNamed(context, '/'),
        child: Text("Logout",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.black54, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Center(
          child: Center(
            child: Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(60.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 155.0,
                          child: Image.asset(
                            "assets/d20-logo.png",
                            fit: BoxFit.contain,
                          ),
                        ),
                          Column(
                            children: <Widget> [
                              SizedBox(
                                height: 35.0,
                              ),
                              Padding(
                                  child: logoutButon,
                                  padding: const EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 0.0)
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                            ],
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
    );
  }
}
