import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:d20_app/routes.dart';

void main() => runApp(D20App());

class D20App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'D20 App',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: routes,
    );
  }
}
