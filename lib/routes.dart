import 'package:flutter/material.dart';
import 'screens/login.dart';
import 'screens/cadastro.dart';
import 'screens/home.dart';


final routes = {
  '/':         (BuildContext context) => new LoginPage(),
  '/cadastro': (BuildContext context) => new CadastroPage(),
  '/home':     (BuildContext context) => new HomePage(),
};