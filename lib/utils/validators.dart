class Validator{
  final String emailPattern = r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
  final String telefonePattern = r'\([0-9]{2}\) [0-9]{4,6}-[0-9]{3,4}$';


  String emailValidator(String value){
    if(value.isEmpty){
      return "Obrigatório";
    }else if(!value.contains(new RegExp(emailPattern))){
      return "Digite um e-mail válido";
    }
    return null;
  }

  String senhaValidator(String value){
    if(value.isEmpty){
      return "Obrigatório";
    }
    return null;
  }

  String nomeValidator(String value){
    if(value.isEmpty){
      return "Obrigatório";
    }
    return null;
  }

  String sobrenomeValidator(String value){
    if(value.isEmpty){
      return "Obrigatório";
    }
    return null;
  }

  String telefoneValidator(String value){
    if(value.isEmpty){
      return "Obrigatório";
    }else if(!value.contains(new RegExp(telefonePattern))){
      return "Digite um número de telefone válido";
    }
    return null;
  }

}