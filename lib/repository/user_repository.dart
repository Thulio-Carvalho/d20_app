import 'package:http/http.dart' as http;
import 'dart:convert';
import '../model/user.dart';

Future<Stream<User>> getUsers() async {
  final String url = 'https://d20-app.herokuapp.com/user';

  final client = new http.Client();
  final streamedRest = await client.send(
      http.Request('get', Uri.parse(url))
  );

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .expand((data) => (data as List))
      .map((data) => User.fromJSON(data));
}